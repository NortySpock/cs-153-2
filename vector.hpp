//////////////////////////////////////////////////////////////////////////////
/// @file vector.hpp
/// @author David Norton :: CS153 Section 1B
/// @brief This is the vector definition file
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/// @fn Vector ()
/// @brief Constructor for the vector class. 
/// Briefly, vectors are scalable arrays.
/// @pre No vector exists
/// @post A vector of some type exists, with no elements in the array,
/// a size of zero and a maximum size of one. 
/// @param None. 
/// @return None (constructor)
////////////////////////////////////////////////////////////////////// 
template <class generic>
Vector<generic>::Vector ()
{
  m_size = 0;
  m_max_size = 1;
  m_data = new generic [m_max_size];
}

//////////////////////////////////////////////////////////////////////
/// @fn Vector (const Vector <generic> &)
/// @brief Copy constructor -- this copies one vector into the other. 
/// Example: Vector <int> b(a) makes 'b' a carbon copy of 'a'
/// @pre A vector exists
/// @post Two vectors now exist, with the exact same data, size 
/// and maximum size. 
/// a size of zero and a maximum size of one. 
/// @param None. 
/// @return None (constructor)
////////////////////////////////////////////////////////////////////// 
template <class generic>
Vector<generic>::Vector (const Vector <generic> &v)
{
  m_size =0;
  m_max_size = 1;
  m_data = new generic [m_max_size];

  *this = v;
}

//////////////////////////////////////////////////////////////////////
/// @fn Vector & operator= (const Vector &)
/// @brief Assigns one vector to the other
/// @pre Two vectors exist.
/// @post The right-most vector is assigned to the left-most vector.
/// @param None.
/// @return The vector it sets.
////////////////////////////////////////////////////////////////////// 
template <class generic>
Vector<generic> & Vector<generic>::operator= (const Vector <generic> &v)
{
  clear();
  for(unsigned int i = 0; i < v.size(); i++)
  {
	push_back(v[i]);
  }
  return *this;
}


//////////////////////////////////////////////////////////////////////
/// @fn ~Vector 
/// @brief Destructor of vector
/// @pre A vector exists
/// @post The vector is completely and totally removed from memory.
/// @param None. 
/// @return None (destructor)
////////////////////////////////////////////////////////////////////// 
template <class generic>
Vector<generic>::~Vector ()
{
  delete [] m_data;
}

//////////////////////////////////////////////////////////////////////
/// @fn void push_back (generic);
/// @brief Adds an element to the end of the vector. 
/// If the vector array is full, 
/// it creates a new vector array twice the size of the old one, copies 
/// everything from the old array to the new array, and deletes 
/// the old array.
/// @pre Vector exists, element to be added is passed as the argument.
/// @post Vector has one more element, the element it was passed.
/// @param The element to be added to the array.
/// @return None. (void)
////////////////////////////////////////////////////////////////////// 
template <class generic>
void Vector<generic>::push_back (generic x)
{
  if(m_size < m_max_size)//element will fit in current vector
  {
    m_data[m_size++] = x;
  }
  else//It ain't gonna to fit
  {   
	//Update the max_size
	m_max_size *= 2;
    
	//Point a temporary pointer at the old data
	generic* temp = m_data;
	
	//Point the data pointer at a new array (now twice the size of the old)
	m_data = new generic [m_max_size ];
	
	//Copy from the old data to the new data array
	for(unsigned int i = 0; i < m_size; i++)
	{
	  m_data[i] = temp[i];
	}
	
	//Delete the old array (very important!)
	delete [] temp;
		
	//Put the new element in
	m_data[m_size++] =  x;
  }
  return; //void
}

//////////////////////////////////////////////////////////////////////
/// @fn void pop_back ();
/// @brief Removes one item from the end of the vector.
/// Note that the item is not actually delete, size is only decreased
/// by one so that the element simply cannot be accessed anymore.
/// If the size of the vector ever becomes less than or equal to
/// one-fourth of max_size, a new vector half the size of the first is
/// created, the data copied over and the old vector deleted.
/// @pre Vector with elements exists
/// @post Vector has one less element in it.
/// @param None.
/// @return None. (void)
////////////////////////////////////////////////////////////////////// 
template <class generic>
void Vector<generic>::pop_back ()
{
  if(m_size <= 0)//Attempting to make size negative.
  {
    throw Exception
    (
      CONTAINER_EMPTY,
      "The container is empty and no more items can be removed."
    );
  }
  
  m_size--;
  
  if(m_size <= (m_max_size/4))//size less than 1/4 max_size
  {
    //Cut max size in half
    m_max_size /= 2;
	
	//Point a temporary pointer at the old data
	generic* temp = m_data;
	
	//Point the data pointer at a new array (now half the size of the old)
	m_data = new generic [m_max_size ];
	
	//Copy from the old data to the new data array
	for(unsigned int i = 0; i < m_size; i++)
	{
	  m_data[i] = temp[i];
	}
	
	//Delete the old array (very important!)
	delete [] temp;
  }

  if(m_size == 0)//If it's now zero
  {
    clear();
  }
  
  return; //void
}

//////////////////////////////////////////////////////////////////////
/// @fn void clear ();
/// @brief Deletes the old vector and creates a brand new one.
/// @pre Vector of arbitrary length exists
/// @post Brand new vector of size zero and max_size one exists
/// @param None.
/// @return None. (void)
////////////////////////////////////////////////////////////////////// 
template <class generic>
void Vector<generic>::clear ()
{
  //Point a temporary pointer at the old data
  generic* temp = m_data;
  
  //Reset max size
  m_max_size = 1;
  //Reset size
  m_size = 0;
  
  //Point the data pointer at a new array of size m_max_size
  m_data = new generic[m_max_size];
  
  //Delete the old array (very important!)
  delete [] temp;
  
  return; //void
}

template <class generic>
generic& Vector<generic>::operator[] (unsigned int x)
{
  if((x >= m_size) || (x < 0)) //Trying to write out of bounds
  {
    throw Exception
    (
      OUT_OF_BOUNDS,
      "You are attempting to write outside the current scope of the array."
    ); 
  }
  return m_data[x];
}

template <class generic>
generic& Vector<generic>::operator[] (unsigned int x) const
{
  if((x >= m_size) || (x < 0)) //Trying to read out of bounds
  {
    throw Exception
    (
      OUT_OF_BOUNDS,
      "You are attempting to read outside the current scope of the array."
    ); 
  }
  return m_data[x];
}

//////////////////////////////////////////////////////////////////////////////
/// @fn unsigned int size ()
/// @brief Gets the current size of the vector.
/// @pre Vector exists
/// @post No change                                           
/// @param None.
/// @return Current size of the vector (zero for no elements in the vector, one
/// for one element currently in the vector, etc.)
//////////////////////////////////////////////////////////////////////////////
template <class generic>
unsigned int Vector<generic>::size () const
{
  return m_size;
}

//////////////////////////////////////////////////////////////////////////////
/// @fn unsigned int max_size ()
/// @brief Gets the max size of the vector.                             
/// @pre Vector exists  
/// @post No change                                                                
/// @param None.
/// @return Maximum size of the vector 
//////////////////////////////////////////////////////////////////////////////
template <class generic>
unsigned int Vector<generic>::max_size () const
{
  return m_max_size;
}
