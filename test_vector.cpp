//////////////////////////////////////////////////////////////////////////////
/// @file test_vector.cpp
/// @author David Norton :: CS153 Section 1B
/// @brief This is the test_vector definition file, which defines 
/// the unit tests for the vector class functions.
//////////////////////////////////////////////////////////////////////////////
#include "test_vector.h"
#include <iostream>
#include <cmath>
using namespace std;

CPPUNIT_TEST_SUITE_REGISTRATION (Test_vector);

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_constructor ();
/// @brief This tests the constructor of the Vector class
/// @pre Vector class should already be declared. 
/// This function will instantiate a vector and make sure the initial 
/// values are correct (size is zero and max_size is zero).
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_vector::test_constructor ()
{

  Vector<int> a;
  CPPUNIT_ASSERT(a.size () == 0);
  CPPUNIT_ASSERT(a.max_size () == 1);

}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_push_back ();
/// @brief This tests the push_back function of the Vector class
/// @pre Vector class should already be declared. 
/// This function will instantiate one and test the push_back function.
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_vector::test_push_back ()
{
  const int TEST_MAX = 5000;
  
  Vector<int> a;
  
  a.push_back(1989);
  CPPUNIT_ASSERT(a[0] == 1989);
  CPPUNIT_ASSERT(a.size () == 1);
  CPPUNIT_ASSERT(a.max_size () == 1);
  
  a.push_back(1956);
  CPPUNIT_ASSERT(a[1] == 1956);
  CPPUNIT_ASSERT(a.size () == 2);
  CPPUNIT_ASSERT(a.max_size () == 2);
  
  for(unsigned int i = 2; i < TEST_MAX; i++)
  {
    a.push_back (i);
	CPPUNIT_ASSERT (a[i] == i);
	CPPUNIT_ASSERT (a.size() == i+1);
	/* Try as I might, I can't come up with an algorithm better than
	 * the one listed in http://web.mst.edu/~buechler/DataStructures/
	 * InClassAssignments/sol/hw2/2010SP.pdf
	 * Sorry.
	 */
	 CPPUNIT_ASSERT (pow(2, floor(log(i)/log(2))+1) == a.max_size ());	 
  }
  
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_pop_back ();
/// @brief This tests the pop_back function of the Vector class
/// @pre Vector class should already be declared. 
/// This function will instantiate a vector, populate it,
/// and then test the pop_back function.
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_vector::test_pop_back ()
{
  const int TEST_MAX = 5000;
  
  Vector<int> a;
  
  try
  {
    a.pop_back();//set size to -1
	CPPUNIT_ASSERT (false);
  }
  catch (Exception & e)
  {
    CPPUNIT_ASSERT (CONTAINER_EMPTY == e.error_code ());
  }
  

  for(unsigned int i = 0; i < TEST_MAX; i++)
  {
    a.push_back (i);
  }
  

  
  int test_max_size = a.max_size();
  //From 4999 to 3
  for(unsigned int i = (a.size()-1); i > 2; i--)
  {
    CPPUNIT_ASSERT (a[i] == i);
  	CPPUNIT_ASSERT ((a.size()-1) == a[i]);
  	CPPUNIT_ASSERT (test_max_size =- a.max_size());
  	a.pop_back();
  	if(a.size() <= (test_max_size/4))
  	{
  	  //Cut max size in half
  	  test_max_size /= 2;
  	}
  }

  //Two
  a.pop_back();
  CPPUNIT_ASSERT (a.size() == 2);
  CPPUNIT_ASSERT (a.max_size() == 4);
  
  //One
  a.pop_back();
  CPPUNIT_ASSERT (a.size() == 1);
  CPPUNIT_ASSERT (a.max_size() == 2);
  
  //Zero
  a.pop_back();
  CPPUNIT_ASSERT (a.size() == 0);
  CPPUNIT_ASSERT (a.max_size() == 1);
  

}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_assignment ();
/// @brief This tests the assignment and copy operators of the Vector class
/// @pre Vector class should already be declared. 
/// This function will instantiate a vector, populate it,
/// then copy it to another vector to test the copy and assignment operators
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_vector::test_assignment ()
{
  const int TEST_MAX = 5000;

  Vector<int> a;

  for(unsigned int i = 0; i < TEST_MAX; i++)
  {
    a.push_back (i);
  }

  
  Vector <int> b(a);
   
  CPPUNIT_ASSERT (a.size() == b.size());
  CPPUNIT_ASSERT (a.max_size() == b.max_size());
  for (unsigned int i = 0; i < b.size(); i++)
  {
    CPPUNIT_ASSERT (a[i] == b[i]);
  }

  
}

//////////////////////////////////////////////////////////////////////
/// @fn test_scope
/// @brief Tests the exception handling for trying to walk off the 
/// vector
/// @pre Need push_back to implement this.
/// @post All handled by the CPPUNIT_TEST
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////
void Test_vector::test_scope ()
{
 
  Vector<int> a;

  const int TEST_MAX = 10;
  
  for(int i = 0; i < TEST_MAX; i++)
  {
    a.push_back(i);
  }
 

 
  //Read
  CPPUNIT_ASSERT (a[3] == 3); 

  //Write
  a[3] = 4;
  CPPUNIT_ASSERT (a[3] == 4);
  
  int temp;
  try //Read negative
  {
    temp = a[-1];
    CPPUNIT_ASSERT (0);
  }
  catch (Exception & e)
  {
    CPPUNIT_ASSERT (OUT_OF_BOUNDS == e.error_code ());
  }
  
  try //Read above size
  {
    temp = a[a.size()+1];
    CPPUNIT_ASSERT (0);
  }
  catch (Exception & e)
  {
    CPPUNIT_ASSERT (OUT_OF_BOUNDS == e.error_code ());
  }


  temp = 3;  
  try //Write negative
  {
    a[-1] = temp;
    CPPUNIT_ASSERT (0);
  }
  catch (Exception & e)
  {
    CPPUNIT_ASSERT (OUT_OF_BOUNDS == e.error_code ());
  }

  try //Write above size
  {
    a[a.size()+1] = temp;
    CPPUNIT_ASSERT (0);
  }
  catch (Exception & e)
  {
    CPPUNIT_ASSERT (OUT_OF_BOUNDS == e.error_code ());
  }

 
  
  return; //void
}

