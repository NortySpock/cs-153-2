//////////////////////////////////////////////////////////////////////////////
/// @file vector.h
/// @author David Norton :: CS153 Section 1B
/// @brief This is the vector header file
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/// @class Vector
/// @brief Makes a vector class with appropriate functions and exception
/// handling routines.
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/// @fn Vector ()
/// @brief Constructor for the vector class. 
/// Briefly, vectors are scalable arrays.
/// @pre No vector exists
/// @post A vector of some type exists, with no elements in the array,
/// a size of zero and a maximum size of one. 
/// @param None. 
/// @return None (constructor)
////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////
/// @fn Vector (const Vector <generic> &)
/// @brief Copy constructor -- this copies one vector into the other. 
/// Example: Vector <int> b(a) makes 'b' a carbon copy of 'a'
/// @pre A vector exists
/// @post Two vectors now exist, with the exact same data, size 
/// and maximum size. 
/// a size of zero and a maximum size of one. 
/// @param None. 
/// @return None (constructor)
////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////
/// @fn ~Vector 
/// @brief Destructor of vector
/// @pre A vector exists
/// @post The vector is completely and totally removed from memory.
/// @param None. 
/// @return None (destructor)
////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////
/// @fn Vector & operator= (const Vector &)
/// @brief Assigns one vector to the other
/// @pre Two vectors exist.
/// @post The right-most vector is assigned to the left-most vector.
/// @param None.
/// @return The vector it sets.
////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////
/// @fn void push_back (generic);
/// @brief Adds an element to the end of the vector. 
/// If the vector array is full, 
/// it creates a new vector array twice the size of the old one, copies 
/// everything from the old array to the new array, and deletes 
/// the old array.
/// @pre Vector exists, element to be added is passed as the argument.
/// @post Vector has one more element, the element it was passed.
/// @param The element to be added to the array.
/// @return None. (void)
////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////
/// @fn void pop_back ();
/// @brief Removes one item from the end of the vector.
/// Note that the item is not actually delete, size is only decreased
/// by one so that the element simply cannot be accessed anymore.
/// If the size of the vector ever becomes less than or equal to
/// one-fourth of max_size, a new vector half the size of the first is
/// created, the data copied over and the old vector deleted.
/// @pre Vector with elements exists
/// @post Vector has one less element in it.
/// @param None.
/// @return None. (void)
////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////
/// @fn void clear ();
/// @brief Deletes the old vector and creates a brand new one.
/// @pre Vector of arbitrary length exists
/// @post Brand new vector of size zero and max_size one exists
/// @param None.
/// @return None. (void)
////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////
/// @fn void clear ();
/// @brief Deletes the old vector and creates a brand new one.
/// @pre Vector of arbitrary length exists
/// @post Brand new vector of size zero and max_size one exists
/// @param None.
/// @return None. (void)
////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn unsigned int size ()
/// @brief Gets the current size of the vector.
/// @pre Vector exists
/// @post No change                                           
/// @param None.
/// @return Current size of the vector (zero for no elements in the vector, one
/// for one element currently in the vector, etc.)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn unsigned int max_size ()
/// @brief Gets the max size of the vector.                             
/// @pre Vector exists  
/// @post No change                                                                
/// @param None.
/// @return Maximum size of the vector 
//////////////////////////////////////////////////////////////////////////////

#ifndef VECTOR_H
#define VECTOR_H

#include "exception.h"

template <class generic>
class Vector
{
  public:
    Vector ();
    Vector (const Vector <generic> &);
    ~Vector (); 
    Vector & operator= (const Vector &);
    void push_back (generic);
    void pop_back ();
    void clear ();
    generic & operator[] (unsigned int);
    generic & operator[] (unsigned int) const;
    unsigned int size () const;
    unsigned int max_size () const;

  private:
    unsigned int m_size; //The number of elements in the vector
    unsigned int m_max_size; //The current maximum number of elements
    generic * m_data; //The pointer to the vector.
};

#include "vector.hpp"
#endif
