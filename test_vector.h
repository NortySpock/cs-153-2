//////////////////////////////////////////////////////////////////////////////
/// @file test_vector.h
/// @author David Norton :: CS153 Section 1B
/// @brief This is the test_vector header file, which defines the unit tests
/// for the vector class functions.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/// @class Test_vector
/// @brief Unit Tests the vector class.
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST_SUITE (Test_vector);
/// @brief This calls the unit testing library for CPP, 
/// using this header file as an argument
/// @pre Requires the indicated header file to exist 
/// @post Reports the success or failure of unit tests.
/// @param Test_array is the header being called
/// @return I believe this is void, instead cout-ing 
/// the results to the terminal
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_constructor);
/// @brief This calls the unit testing library for CPP, 
/// using the test_constructor member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_constructor is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_push_back);
/// @brief This calls the unit testing library for CPP, 
/// using the test_push_back member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_push_back is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_pop_back);
/// @brief This calls the unit testing library for CPP, 
/// using the test_pop_back member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_pop_back is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_assignment);
/// @brief This calls the unit testing library for CPP, 
/// using the test_assignment member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_assignment is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_scope);
/// @brief This calls the unit testing library for CPP, 
/// using the test_scope member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_scope is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_constructor ();
/// @brief This tests the constructor of the Vector class
/// @pre Vector class should already be declared. 
/// This function will instantiate a vector and make sure the initial 
/// values are correct (size is zero and max_size is zero).
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_push_back ();
/// @brief This tests the push_back function of the Vector class
/// @pre Vector class should already be declared. 
/// This function will instantiate one and test the push_back function.
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_pop_back ();
/// @brief This tests the pop_back function of the Vector class
/// @pre Vector class should already be declared. 
/// This function will instantiate a vector, populate it,
/// and then test the pop_back function.
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_assignment ();
/// @brief This tests the assignment and copy operators of the Vector class
/// @pre Vector class should already be declared. 
/// This function will instantiate a vector, populate it,
/// then copy it to another vector to test the copy and assignment operators
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_scope (); 
/// @brief This tests the exception-handling capabilities of the bracket [] 
/// operators, making sure that you cannot read or write outside of the vector
/// @pre Vector class should already be declared. 
/// This function will instantiate a vector, populate it,
/// then check to make sure exceptions are thrown when you try to read or write
/// anywhere outside of the vector.
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

#ifndef TEST_VECTOR_H
#define TEST_VECTOR_H

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/config/SourcePrefix.h>

#include "vector.h"
//#include "exception.h"

class Test_vector : public CPPUNIT_NS::TestFixture
{
  private:
    CPPUNIT_TEST_SUITE (Test_vector);
      CPPUNIT_TEST (test_constructor);
      CPPUNIT_TEST (test_push_back);
      CPPUNIT_TEST (test_pop_back);
      CPPUNIT_TEST (test_assignment);
      CPPUNIT_TEST (test_scope);
    CPPUNIT_TEST_SUITE_END ();

  protected:
  void test_constructor ();
  void test_push_back ();
  void test_pop_back ();
	void test_assignment ();
	void test_scope (); 
};

#endif
